<?php
require_once 'custom.php';


/**
 * Gets the letters on the board.
 * To analyze this content, see the class BoardAnalyzer().
 */
class BoardReader {
    
    
    /**
     * Returns the letters placed on the game board
     * @return array See the JSON file to know the structure of the data
     */
    function getBoard() {

       return json_decode(file_get_contents(custom('boardPath')), true);
    }


    /**
     * Memorize the new letters placed by the player which touch letters placed 
     * during the previous turns
     * 
     * @param array $currentBoard The existing game board, without the letters added by the player
     *                           Example : [
     *                                      '8_5'  => 'C',
     *                                      '7_5'  => 'E',
     *                                      '10_5' => 'O'
     *                                      ]
     * @param array $newLetters The letters added by the player
     *                          (same data structure)
     * @return type
     */
    function getNewLettersWithBoardNeighbor($currentBoard, $newLetters) {
        
        $result = null;
        
        foreach($newLetters as $stringCoords=>$letter) {
            
            list($x, $y) = explode('_', $stringCoords);

            if( isset($currentBoard[($x-1).'_'.$y]) or isset($currentBoard[($x+1).'_'.$y]) or
                isset($currentBoard[$x.'_'.($y-1)]) or isset($currentBoard[$x.'_'.($y+1)])
                ) {
                $result[$x.'_'.$y] = $letter;
            }
        }
        
        return $result;
    }
    
    
    /**
     * Checks whether one of the given letters is placed in the central cell of the board.
     * (The first word of the game must cross this cell, see Popotamo's rules)
     * 
     * @param array $letters The letters with coordinates [x_y=>letter, ...]
     * @return boolean
     */
    function isLetterInCenter($letters) {
        
        $centralCoords = $this->getCentralStringCoords();
        
        return (isset($letters[$centralCoords])) ? true : false;
    }
    
    
    /**
     * Returns the coordinates of the central cell of the game board
     * 
     * @return string The coordinates as a string. Example : "8_10"
     */
    function getCentralStringCoords() {
        
        $centralX = (int)floor(custom('boardCols')/2);
        $centralY = (int)floor(custom('boardRows')/2);
        
        return $centralX.'_'.$centralY;
    }
    
    
    /**
     * Checks whether one of the given letters is in the first or last colums/rows of the game board.
     * (The Popotamo's rules say that the board must be cleaned in this case)
     * 
     * @param array $letters The letters with coordinates [x_y=>letter, ...]
     * @param int $nbrCols  The width of the game board
     * @param int $nbrRows  The height of the game board
     */
    function isLetterInBorder($letters) {
        
        $result = false;
        
        foreach($letters as $stringCoord=>$letter) {
            
            list($x, $y) = explode('_', $stringCoord);
            $x = (int)$x;
            $y = (int)$y;

            if($x === 0 or $x === custom('boardCols')-1 or
               $y === 0 or $y === custom('boardRows')-1
                ) {
                $result = true;
                break;
            }
        }
        
        return $result;
    }
    
    
    /**
     * Checks that the new letters are not placed in already occupied cells
     * 
     * @param array $currentBoard The game board, as returned by the method getBoard()
     * @param array $newLetters   The letters placed by the player, with coordinates 
     *                            [x_y=>letter, ...]
     * @return boolean "True" if one or more cells are occupied, else "False"
     */
    function areCellsVacant($currentBoard, $newLetters) {
        
        return empty(array_intersect_key($currentBoard, $newLetters)) ? true : false;
    }
}
