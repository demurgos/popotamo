<?php
/**
 * Probabilité de sortie des lettres lors d'une pioche (en pourcentages)
 * (source : http://www.popotamo.com/forum/thread/1326703)
 */
function letters() {

    return [
        'E' => 14.73,
        'S' => 10.01,
        'A' => 9.72,
        'I' => 9.41,
        'R' => 8.52,
        'N' => 7.25,
        'T' => 6.83,
        'O' => 5.96,
        'L' => 4.03,
        'U' => 3.67,
        'C' => 3.47,
        'M' => 2.55,
        'P' => 2.34,
        'D' => 2.33,
        'G' => 1.64,
        'B' => 1.44,
        'F' => 1.30,
        'H' => 1.23,
        'Z' => 1.05,
        'V' => 0.93,
        'Q' => 0.52,
        'Y' => 0.40,
        'X' => 0.27,
        'J' => 0.17,
        'K' => 0.09,
        'W' => 0.02,
        ];
}
