<?php
class DictReader {
    
    
    private $dicts = [];
    
    
    /**
     * Checks whether the word exists in the popodico
     * 
     * @param string $word The word to check
     * @return boolean
     */
    function isInDict($word) {
        
        $initial = $word[0];
        // Loads the adapted letter of the dictionary
        if(!isset($this->dicts[$initial])) {
            $dicts[$initial] = $this->getDictWords($initial);
        }
        
        return in_array(strtoupper($word), $dicts[$initial]) ? true : false;
    }
    
    
    /**
     * Loads the appropriate letter of the dictonary (the dictionary is cut out 
     * in 1 file by first letter for better perfomancesà
     * 
     * @param string $unsafeLetter The first letter of the word (A, B, C...)
     * @return array The words of the dictionary for the given letter
     */
    private function getDictWords($unsafeLetter) {
        
        // Sanitize the letter, as it comes from a word sent by the player
        $letter = filter_var($unsafeLetter, FILTER_VALIDATE_REGEXP, ['options' => ['regexp'=>'/^[A-z]$/'] ]);
        
        if($letter === false) {
            return false;
        }
        else {            
            $dictPath = dirname(__DIR__).'/_POPODICO/cut-by-letter/';
            $fileName = strtoupper($letter).'.txt';
            return file($dictPath.$fileName, FILE_IGNORE_NEW_LINES);
        }
    }
}
