<?php
require_once 'custom.php';

/**
 * Get the data of the chat for further display
 */
class ChatReader {
    
    
    /**
     * Call this method to get the sanitized data of the chat
     * @return array
     */
    function getChatEvents() {
        
        // Get the raw data of the chat (warning : not sanitized !)
        $unsafeChatEvents = json_decode( file_get_contents(custom('chatPath')) , true );
        // Sanitize the data
        return $this->sanitizeChatEvents($unsafeChatEvents);
    }
    
    
    /**
     * Sanitize the data of the tchat befor duisplaying them
     * 
     * @param array $unsafeChatEvents All the revents of the chat, as extracted 
     *                               from the JSON file
     * @return array The sanitized data, in the same structure
     */
    private function sanitizeChatEvents($unsafeChatEvents) {
        
        $sanitizedChatEvents = [];
        
        foreach($unsafeChatEvents as $event) {
            
            // The common elements to all the possibile events in the chat
            $sanitizedCommon = [
                'eventType'  => $event['eventType'],
                'datetime'   => $event['datetime'],
                'authorName' => filter_var($event['authorName'], FILTER_SANITIZE_SPECIAL_CHARS),
                ];
            
            // The specific elements, depending on the type of event
            if($event['eventType'] === 'newWords') {
                $sanitizedSpecial = ['newWords' => filter_var_array($event['newWords'], FILTER_SANITIZE_STRING)];
            }
            elseif($event['eventType'] === 'message') {
                $sanitizedSpecial = ['message' => filter_var($event['message'], FILTER_SANITIZE_SPECIAL_CHARS),];
            }
            
            $sanitizedChatEvents[] = $sanitizedCommon + $sanitizedSpecial;
        }
        
        return $sanitizedChatEvents;
    }
}
