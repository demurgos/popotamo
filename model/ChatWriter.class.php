<?php
/**
 * Save the game events in the history of the chat 
 * (new words placed, messages posted)
 */
class ChatWriter {
    
    
    /**
     * Save the new words placed
     * 
     * @param string $authorName the pseudo of the player who placed the words
     * @param array $newWords The list of words placed [word1, word2, ...]
     */
    function addEventNewWords($authorName, $newWords) {
        
        // Respect the structure of the JSON file, otherwise it won't be read 
        // correctly by the ChatReader() class
        $newEvent = [
            'eventType'  => 'newWords',
            'datetime'   => date('c'),
            'authorName' => filter_var($authorName, FILTER_SANITIZE_SPECIAL_CHARS),
            ];
        
        foreach($newWords as $newWord) {
            $newEvent['newWords'][] = [
                'word'   => filter_var($newWord, FILTER_SANITIZE_STRING),
                'points' => 0,
                ];
        }
        
        $this->addEvent($newEvent);
    }
    
    
    /**
     * Writes the new event at the end of the JSON file storing the chat
     * 
     * @param array $newEvent The data of the nex event, as returned by addEventNewWords()
     */
    private function addEvent($newEvent) {
                
        $chatEvents = json_decode( file_get_contents(custom('chatPath')) , true );        
        $chatEvents[] = $newEvent;
        
        file_put_contents(custom('chatPath'), json_encode($chatEvents));
    }
}
