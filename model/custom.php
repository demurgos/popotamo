<?php
/**
 * Centralize here the common variables to avoid duplications
 * 
 * @param string $item
 * @return string
 */
function custom($item) {
    
    // The file which contains the letters of the board
    $result = [
        'boardPath'     => dirname(__DIR__).'/_DATA/board.json',
        'boardCols'     => 19,
        'boardRows'     => 19,
        'chatPath'      => dirname(__DIR__).'/_DATA/chat.json',
        ];
    
    return $result[$item];
}

