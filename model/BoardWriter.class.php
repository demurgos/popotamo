<?php
require_once 'custom.php';

class BoardWriter {
    
    /**
     * Saves the new board in the database/file
     * 
     * @param array $newBoard All the letters of the board (former+new letters)
     *                        Each letter must be indexed by coordinates, example :
     *                        [ 2_10 => S, 3_10 => I, ... ]
     * @return boolean
     */
    function updateBoard($newBoard) {
        
        return file_put_contents(custom('boardPath'), json_encode($newBoard));
    }
}
