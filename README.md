# Popotamo/Street writer

## Description / spécifications 
(synthèse des discussions sur le discord d'Eternal Twin)

Le jeu est relativement simple : http://www.popotamo.com/help   
NB : une copie de cette page a été saugardée sur le gitlab popotamo-extra : https://gitlab.com/eternal-twin/popotamo/popotamo-extra/-/tree/master/game-rules

## Fonctionnalités minimales
Basiquement, c'est un scrabble. Mais tous les joueurs jouent en même temps ! Ce n'est pas un jeu en tour par tour. 

Déroulement d'une partie :
  * La partie démarre quand 5 joueurs l'ont rejointe (ou 4 : v. plus bas "Précisions sur les règles")
  * Le joueur pioche des lettres avec le bouton "Piocher". Un joueur ne peut piocher plus d 6 lettres. améliorations possibles (A DOCUMENTER).
  * Le joueur pose ses lettres, puis valide son mot avec le bouton "Popotamo". règles de pose :
    - Les nouvelles lettres posées doivent passer par au moins 1 lettre déjà sur le plateau.
	- Le joueur peut poser autant de lettres qu'il veut (pas de tour par tour).
	- Le joueur peut aussi compléter une mot déjà sur le plateau (ex : "parti"+"es")
  * Quand un joueur doit piocher, un compte à rebours se déclenche. Sa durée dépend de la compétence "rapidité" du joueur :
    - 1 point de rapidité = 1 pioche toutes les 20 minutes
    - 10 points de rapidité = 1 pioche toutes les 10 minutes    
	 NB : en mode "Rapide", les délais sont plus courts d'en mode "Normal"
  * Le 1er mot doit passer par la case centrale du plateau
  * Si un mot touche le bord du plateau, ce dernier est entièrement nettoyé et le joueur perd 10 points.
  * Lorsqu'un joueur atteint le nombre de points demandé il gagne la partie, les autres joueurs continuent à jouer jusqu'à ce que 4 joueurs aient atteint ce nombre. 

**A prévoir en termes de code :**
  * Une grille en tableau HTML
  * Une zone d'inventaire affichant les lettres du joueurs + 1 bouton "Piocher"
  * Clic sur une lettre pour la sélectionner, clic sur la case où la poser. On peut annuler la pose en cliquant à nouveau sur la lettre sur la grille pour la remettre dans l'inventaire. 
  * Le serveur vérifie si les lettres posées forment un mot présent dans le dictionnaire du jeu. Implications en code :
	- Chaque nouvelle lettre posée doit être mémorisée temporairement, avec ses coordonnées
	- Au moment de la vérification, le serveur remet les lettres dans l'ordre en les triant par coordonnées
	- Vérifier que ne forme pas de mot inconnu avec les autres cases adjacentes
	  => Si pas de mot formé, afficher un message au joueur pour lui demander de changer son mot.
    => Si un mot est formé, ajouter les points au joueur. Points gagnés = Nombre de lettres déjà posées + Nombre de nouvelles lettres au carré (v. le point 4 de l'aide officielle pour le détail)

  * Dans Popotamo, un algorithme règle la pioche des lettres et fait qu'on a peu de chance de tirer 8 consonnes ou 8 voyelles ou kwqj dans une pioche, contrairement à Street writer où ce genre de chose n'est pas rare !


## Fonctionnalités avancées
  * Rythmes de jeu :
	- Mode normal = ???
	- Mode rapide = ???
  * Partie à thèmes = seuls les mots du thème sont acceptés
  * Classement des joueurs par points
  * Compétences à acquérir
   
  * En plus des parties "normales" ou "à thèmes" nous avons des parties "énigmes " ou "tournoi double " ou "tournoi simple". Pour ces parties là, nous jouons en équipe en choisissant nos partenaires et sur une base de mots pré-rentrés par Bouillegri et qui rougiront. Cf topic  Forum "Tournois et Énigmes" :
	http://www.popotamo.com/forum/8
   
   
## Précisions sur les règles

**Commencer une partie :** une partie se joue normalement à 5, mais s'il y a un trop long temps d'attente, on passe à 4 (au bout de 10 mn).  
Le joueur  qui s'inscrit sur une partie ne connait pas ses adversaires  jusqu’au lancement de la partie.  
Tant qu'une partie n'et pas démarrée, le joueur qui s'est inscrit  peut à tout moment se désengager. 


## Besoins graphiques
Après la mise en service de la V1 fonctionnelle, il faudra ajouter quelques effets visuels pour rendre le jeu plus vivant :
   * Déplacement des lettres par glisser-déposer (plus attractif  qu'un simple "je clique sur la lettre, elle s'efface de l'inventaire et s'affiche sur la grille"). Attention cependant, pour le mobile la méthode en 2 clics de Popotamo est sans doute plus adaptée...
   * Effet visuel sur la grille quand un mot est formé (clignotement...)
   * Effet visuel lors du gain de points   


## Ressources disponibles

=> Site de joueur expliquant des mécaniques du jeu : http://klariaweb.free.fr/popotamo/

=> Popotamo sur Webarchive : https://web.archive.org/web/20071020030742/http://pub.motion-twin.com/popotamo/fr/

=> Nous avons déjà la base de mots du jeu (le "popodico") : dans le dépôt "popotamo-extra" sur le Gitlab Eternal Twin. Il est basé sur l'ODS 5 (Officiel Du Scrabble version 5). L'ODS est à la version 8 en 2020, mais un souci de droits d'auteur n'a pas permis à Motion Twin de l'actualiser.
Un formulaire de recherche manuel est disponible sur ce site : http://patatoide.com/popodico/  
Contacts : sur Discord : @bouillegri#3812, @lettris#6826, @sissouf#3660

=> Autre base de mots envisageable : http://www.lexique.org/  

=> Les joueurs avaient aussi fait programme "dico" qui leur permettait d'entrer tous les mots que nous désirions pour y entrer des mots des parties à thèmes  qui rougissaient et doublaient la valeur des points obtenus sur les parties.

=> Ressources forum Popotamo (à sauvegarder avant fermeture du site !!)
   * Conseils sur le placement des mots, les stratégies... : http://www.popotamo.com/forum/thread/1326703
   * Et les autres post-it de https://fr.scrabblecheat.org/anagramme
