<?php
require_once 'model/BoardReader.class.php';
require_once 'model/ChatReader.class.php';
require_once 'model/custom.php';
require_once 'view/Html.class.php';
require_once 'view/HtmlChat.class.php';
require_once 'controller/Random.class.php';

//Letters already placed on the grid
$boardReader = new BoardReader();
$chatReader  = new ChatReader();
$Html        = new Html();
$htmlChat    = new HtmlChat();
$Random      = new Random();
?>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Popotamo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="resources/main.css?v=1.8">
    </head>
    
    <body>
        <header>
            <img src="resources/img/banner.gif" alt="Popotamo - Un jeu de mots !">
        </header>

        <main>
            <div id="leftColumn">
                <?php
                echo $Html->inventory($Random->getRandomLetters(9));
                echo $Html->board(custom('boardCols'), custom('boardRows'), $boardReader->getBoard());
                ?>
            </div>
            <div id="rightColumn">
                <div id="buttons">
                    [Piocher]
                    <input id="validateWords" type="button" value="Popotamo">
                </div>
                <div id="competences">[A programmer : competences]</div>
                <div id="players">[A programmer : les joueurs de la partie]</div>
                <div id="chat">
                    <?php echo $htmlChat->chat($chatReader->getChatEvents()) ?>
                </div>
            </div>
        </main>

        <footer>
            <a href="https://discord.gg/ERc3svy">Discord</a> · 
            <a href="https://wiki.eternal-twin.net/popotamo">Wiki</a> · 
            <a href="https://gitlab.com/eternal-twin?utf8=%E2%9C%93&filter=popotamo">Code source</a> · 
            <a href="http://www.popotamo.com/">Popotamo original</a>
        </footer>
    </body>
    
    <script src="resources/scripts.js?v=1.6"></script>
    <script src="resources/ApiCaller.js?v=1.2"></script>
    
</html>
