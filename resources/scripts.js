var serverRoot = "http://popotamo";

var letterToPlace = null;
var placedLetters = {};


function setLetterToPlace(letter) {    
    letterToPlace = letter;
}


function setLetterDestination(x, y) {
    
    if (letterToPlace !== null) {
        document.getElementById("coord_"+x+"_"+y).innerHTML = "<div class=\"letter\">"+letterToPlace+"</div>";
        placedLetters[x+"_"+y] = letterToPlace;
        // Remove the letter from the inventory
        document.getElementById("inventory"+letterToPlace).remove();
        // Can't click on multiple cells to place the letter
        letterToPlace = null;
    }
}


/**
 * Sends to the server the new letters placed, to validate the new words
 */
function sendNewLetters() {
    let apiResult = sendData("POST", "validate-words", "newLetters="+JSON.stringify(placedLetters));
    apiResult.then(function(api) {
        if(api.errorCode === "wordsNotInDict") {
            alert("Le mot « "+api.data["wordsNotInDict"][0]+" » n'est pas dans le dictionnaire !");
        } 
        else if(api.errorCode === "lettersInOccupiedCells") {
            alert("Vous ne pouvez pas placer des lettres sur des cases déjà occupées.");
        }
        else if(api.errorCode === "lettersNotConnected") {
            alert("Certaines lettres ne sont pas connectées aux mots déjà posés !");
        }
        else if(api.errorCode === "mustStartInCenter") {
            alert("La case centrale du plateau doit contenir une lettre !");
        }
        else if(api.errorCode === "onlyOneLetter") {
            alert("Vous devez former au moins 1 mot !");
        }
        else {
            for(let newWord of api.data.newWords) {
                document.getElementById("chat").innerHTML += "<p><strong>Joueur inconnu</strong> \
                                   pose « <strong class=\"green\">"+newWord+"</strong> » (0 points)</p>";
                placedLetters = {};
            }
        }
    });
}


// When the player clicks on the "Popotamo" button
document.getElementById("validateWords").addEventListener("click", function() {
    // Don't call the server if the player has placed no letter on the board 
    if(Object.values(placedLetters).length >= 1) {
        sendNewLetters();
    }
});

