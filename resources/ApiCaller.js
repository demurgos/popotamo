/**
 * This is a small lib to call an API and get the returned response
 */

/**
 * Sends data to a PHP script with the GET or POST method
 * 
 * @param {string} method     The HTTP method to send the data (GET or POST)
 * @param {string} scriptName The name of the PHP script to call
 * @param {string} params     The additional parameters to send, as a unique string
 *                            E.g.: "action=get&citizen_id=87"
 */
 async function sendData(method, scriptName, params) {
    
    let apiUrl = `api/${scriptName}.php`,
        option = {
            method: method,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
    if(method==="GET") {
        apiUrl += "?"+params;
    } else {
        option.body= params;
    }
    
    // For debugging : uncomment this line to watch in the browser console 
    // how many times you call the distant APIs, and eventually optimize the redundant calls
//    console.log("API call: "+scriptName);
    
    return await fetch(apiUrl, option).then(toJson);
}


/**
 * Converts a string to JSON and prints the malformed JSON in the console
 */
async function toJson(apiResult) {
    
    try {
        //.text() pour retourner un texte brut, ou .json() pour parser du JSON
        return await apiResult.clone().json();
    } catch (e) {
        await apiResult.clone().text().then(apiResult=>{
            console.error(e);
            console.groupCollapsed("See the result returned by the API:");
            console.log(apiResult);
            console.groupEnd();
            throw e;
        });
    }
}
