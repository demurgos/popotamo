<?php
/**
 * Generates the HTML elements specific to the chat zone
 * (words placed + messages posted)
 */
class HtmlChat {
    
    /**
     * Call this method to gat all the HTML of the chat (new words + messages)
     * 
     * @param array $chatEvents All the events of the chat, as return by the class ChatReader->getChat()
     * @return string HTML
     */
    function chat($chatEvents) {
        
        $result = '';
        
        foreach($chatEvents as $event) {
            
            if($event['eventType'] === 'newWords') {
                $result .= $this->chatEventNewWords($event['datetime'], $event['authorName'], $event['newWords']);
            }
            elseif($event['eventType'] === 'message') {
                $result .= $this->chatEventMessage($event['datetime'], $event['authorName'], $event['message']);
            }
            else {
                $result .= $this->chatEventUnknown($event['eventType']);
            }
        }
        
        return $result;
    }
    
    
    /**
     * Displays the date of an event in the chat
     * 
     * @param string $isoDate Must be a date in the ISO 8601 format.
     *                        Example : "2020-12-29T15:15:13+00:00"
     * @return string HTML
     */
    private function date($isoDate) {
        
        $dt = new DateTime($isoDate);
        return '<span class="date">'.$dt->format('H:i').'</span>';
    }
    
    
    private function chatEventNewWords($utcDate, $authorName, $newWords) {
        
        $htmlNewWords = [];
        $totalPoints  = 0;
        
        foreach($newWords as $word) {
            $htmlNewWords[] = '<strong class="green">'.ucfirst($word['word']).'</strong>';
            $totalPoints += $word['points']; 
        }
        
        return '<p>'.$this->date($utcDate).' <strong>'.$authorName.'</strong> pose « '.join(', ', $htmlNewWords).' » '
             . '('.$totalPoints.' points)</p>';
    }
    
    
    private function chatEventMessage($utcDate, $authorName, $message) {
        
        return '<p class="green"><strong>'.$authorName.'</strong>: '.$message.'</p>';
    }
    
    
    private function chatEventUnknown($eventType) {
        
        return '<p style="color:red">[Bug] Élément de tchat inconnu ("'.$eventType.'")</p>';
    }
}

