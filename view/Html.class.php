<?php
class Html
{
    
    /**
     * Generates the game board
     * @param int   $nbrCols
     * @param int   $nbrRows
     * @param array $boardLetters The letters already placed on the board
     * @return type
     */
    function board($nbrCols, $nbrRows, $boardLetters) {

        $table = '';

        // Generates the lines (each loop = 1 new line)
        for($row=0; $row<$nbrRows; $row++) {

            // Generates the cells inside the line (each loop = 1 new cell)
            $cells = '';
            for($col=0; $col<$nbrCols; $col++) {
                
                $cell_content = (isset($boardLetters[$col.'_'.$row])) 
                                ? '<div class="letter">'.$boardLetters[$col.'_'.$row].'</div>'
                                : '&nbsp;';
                
                // Mark the central cell with a different color
                $class = ($col === (int)floor($nbrCols/2) and $row === (int)floor($nbrRows/2)) ? 'centralCell' : '';
                
                $cells .= '<td id="coord_'.$col.'_'.$row.'" class="'.$class.'" onclick="setLetterDestination(\''.$col.'\', \''.$row.'\')">
                        '.$cell_content.'
                        </td>';
            }

           // Saves the generated line for later display
            $table .= '<tr>'.$cells.'</tr>';
        }

        // Displays the generated map
        return '<table id="board">'.$table.'</table>';
    }
    
    
    function inventory($letters) {
        
        $html = '';
        foreach($letters as $letter) {
            $html .= '<input type="submit" value="'.$letter.'" id="inventory'.$letter.'" class="letter" onclick="setLetterToPlace(this.value)">';
        }
        
        return '<form method="post" id="inventory" onsubmit="return false">
                '. $html .'
            </form>';
    }
}
