<?php
/**
 * Searches the words formed on the game board
 */
class BoardValidator
{
    
    public  $futureBoard  = [];
    private $validWords   = [];
    private $testedCoords = [ 'vertical' => [], 'horizontal' => [] ];    
    
    
    /**
     * @param array $currentBoard The existing game board, without the letters added by the player
     *                           Example : [
     *                                      '8_5'  => 'C',
     *                                      '7_5'  => 'E',
     *                                      '10_5' => 'O'
     *                                      ]
     * @param array $newLetters The letters added by the player
     *                          (same data structure)
     */
    function __construct($currentBoard, $newLetters) {
        
        $this->currentBoard = $currentBoard;
        $this->newLetters   = $newLetters;
        // The game board with all the letters (newly placed + existing)
        $this->futureBoard  = $currentBoard + $newLetters;
    }
    
    
    /**
     * Starting from the letters placed by the player, gets recursively all the words 
     * connected to these letters
     * 
     * @param array $withBoardNeighbor The letters placed by the player which touch 
     *                                 any letter alrdeady on the board. As returned
     *                                 by the method getNewLettersWithBoardNeighbor()
     * @return array
     */
    function getNewWords($withBoardNeighbor) {
        
        foreach($withBoardNeighbor as $stringCoords=>$letter) {
            
            list($x, $y) = explode('_', $stringCoords);
            $this->getNewWord('horizontal', $x, $y);
            $this->getNewWord('vertical', $x, $y);
        }
        
        return $this->validWords;
    }
    
    
    /**
     * Returns the new letters not connected to any already placed word
     * (not allowed by the rules)
     * 
     * @return array Example : [4_7 => F, 10_3 => L, ...]
     */
    public function getNotConnectedLetters() {
        
        $notConnected = null;
        
        $notConnectedCoords = array_diff(array_keys($this->newLetters), $this->testedCoords['horizontal']
                                                                        + $this->testedCoords['vertical']);
        
        foreach($notConnectedCoords as $stringCoord) {
            $notConnected[$stringCoord] = $this->newLetters[$stringCoord];
        }
        
        return $notConnected;
    }
    
    
    /**
     * Starting from the coordinates of any letter, gets the eventual word formed
     * 
     * @param string $direction "horizontal" to search an horizontal word
     *                          "vertical" to search a vertical word
     * @param int $x The X coord (column) of any letter of the word
     * @param int $y The Y coord (line) of the same letter
     * @param string $inRecursion Set its value to "inRecursion" when the method 
     *                            is called inside the method getHorizontalWord()
     * @return array List of the letters of the horizontal word found
     */
    private function getNewWord($direction, $x, $y, $inRecursion=false) {
        
        // Don't test the same letter several times
        if($this->isInTestedCoords($direction, $x, $y)) {
            return;
        }
        
        // In a recursion, stop as soon as we meet a word placed in a former turn
        if($inRecursion === 'inRecursion' and $this->isInFormerWord($direction, $x, $y)) {
            return;
        }
        
        // Search a word in the given direction (horizontal/vertical)
        $newWord = $this->getWordLetters($direction, $x, $y);        
        // Memorize the word found
        $this->addValidWord($newWord);        
        // Memorize the letters already analyzed to avoid infinite recursion (see below) 
        $this->addLettersToTestedCoords($direction, $x, $y, $newWord);        
        // Recursion: search the eventual words crossing the initial word
        $this->getCrossingWords($direction, $x, $y, $newWord);
    }
    
    
    /**
     * Recursion: search all the words crossing the given word 
     * 
     * @param string $direction "horizontal" if the *given word* is horizontal
     *                          "vertical" if the *given word* is vertical
     * @param int $x
     * @param int $y
     * @param array $wordLetters
     */
    private function getCrossingWords($direction, $x, $y, $wordLetters) {
        
        if($direction === 'horizontal') {
            foreach($wordLetters as $x=>$letter) {
                // With an horizontal word, we search vertical crossing words
                $this->getNewWord('vertical', $x, $y, 'inRecursion');
            }
        }
        elseif($direction === 'vertical') {
            foreach($wordLetters as $y=>$letter) {
                $this->getNewWord('horizontal', $x, $y, 'inRecursion');
            }
        }
    }
    
    
    /**
     * Search a word starting from a given letter, in the given direction
     * 
     * @param string $direction "horizontal" to search a word horizontally
     *                          "vertical" to search vertically
     * @param int $x The X coordinate of the letter
     * @param int $y the Y coordinate of the same letter
     * @return array The letters of the found word (not sorted)
     */
    private function getWordLetters($direction, $x, $y) {
        
        if($direction === 'horizontal') {
            $variableCoord = $x;
        } 
        elseif($direction === 'vertical') {
            $variableCoord = $y;
        }
        
        // Search the new letters to the left (if horizontal) or to the top (if vertical)
        for($i=0; $this->isInFutureBoard($direction, $x, $y, $i); $i--) {
            // Index by the X coord to allow sorting the letters horizontally
            // (or by the Y to sort vertically)
            $newWord[$variableCoord+$i] = $this->getFutureBoardLetter($direction, $x, $y, $i);
        }
        // Search the new letters to the right (if horizontal) or to the bottom (if vertical)
        for($i=0; $this->isInFutureBoard($direction, $x, $y, $i); $i++) {
            $newWord[$variableCoord+$i] = $this->getFutureBoardLetter($direction, $x, $y, $i);
        }
        
        // Put the letters of the word in the good order
        ksort($newWord);
        
        return $newWord;
    }
    
        
    /**
     * Determines if a letter exists in the given coordinates
     * 
     * @param string $direction "horizontal" or "vertical"
     * @param int $x
     * @param int $y
     * @param int $i Add $i to the $x or $y coordinate
     * @return bool "True" if there is a letter in the cell
     */
    private function isInFutureBoard($direction, $x, $y, $i) {
        
        if($direction === 'horizontal') {
            return isset($this->futureBoard[($x+$i).'_'.$y]);
        }
        elseif($direction === 'vertical') {
            return isset($this->futureBoard[$x.'_'.($y+$i)]);
        }
    }
    
    
    /**
     * Gets the letter placed in the given coordinates
     * 
     * @param string $direction "horizontal" or "vertical"
     * @param int $x
     * @param int $y
     * @param int $i Add $i to the $x or $y coordinate
     * @return string The letter in this cell
     */
    private function getFutureBoardLetter($direction, $x, $y, $i) {
        
        if($direction === 'horizontal') {
            // With an horizontal word, we move along the X axis
            return $this->futureBoard[($x+$i).'_'.$y];
        }
        elseif($direction === 'vertical') {
            return $this->futureBoard[$x.'_'.($y+$i)];
        }
    }
    
    
    /**
     * Memorize the letters of the given word as tested
     * 
     * @param string $direction "horizontal" or "vertical" according to the direction
     *                          of the word
     * @param int $x The X coordinate of any letter of the word
     * @param int $y The Y coordinate of the same letter
     * @param string $wordLetters The letters composing the word
     *                            For an horizontal word : [X=>letter, X=>letter, ...]
     *                            For a vertical word :    [Y=>letter, Y=>letter, ...]
     */
    private function addLettersToTestedCoords($direction, $x, $y, $wordLetters) {
        
        if($direction === "horizontal") {
            foreach($wordLetters as $x=>$letter) {
                array_push($this->testedCoords[$direction], $x.'_'.$y);
            }
        }
        elseif($direction === "vertical") {
            foreach($wordLetters as $y=>$letter) {
                array_push($this->testedCoords[$direction], $x.'_'.$y);
            }
        }
    }
    
    
    /**
     * Determines if the given letter is inside a word already existing on the board
     * (before the player places his letters)
     * 
     * @param type $direction "horizontal" to search an horizontal word
     *                        "vertical" to search a vertical word
     * @param int $x The X coordinate of the letter
     * @param int $y The Y coordinate of the same letter
     * @return bool
     */
    private function isInFormerWord($direction, $x, $y) {
        
        if($direction === 'vertical') {
            return isset($this->currentBoard[$x.'_'.($y-1)]) or
                   isset($this->currentBoard[$x.'_'.($y+1)]);
        }
        elseif($direction === 'horizontal') {
            return isset($this->currentBoard[($x-1).'_'.$y]) or
                   isset($this->currentBoard[($x+1).'_'.$y]);
        }
    }
    
    
    /**
     * Determines if an horizontal or vertical word has already been searched
     * starting from the given coordinates
     * 
     * @param string $direction "horizontal" or "vertical" to know if 
     *                          an horizontal or vertical word has been already searched
     * @param int $x The X coordinate of the letter
     * @param int $y The Y coordinate of the same letter
     * @return bool
     */
    private function isInTestedCoords($direction, $x, $y) {
        
        return in_array($x.'_'.$y, $this->testedCoords[$direction]) ? true : false;
    }
    
    
    /**
     * Memorize a new word found on the grid
     * 
     * @param array $wordLetters The letters composing the word
     *                           For an horizontal word : [X=>letter, X=>letter, ...]
     *                           For a vertical word :    [Y=>letter, Y=>letter, ...]
     */
    private function addValidWord($wordLetters) {
        
        // A word can't be a single letter
        if(count($wordLetters) > 1) {
            // The words will be returned in lower case (more esthetic)
            $this->validWords[] = strtolower(join($wordLetters));
        }
    }
}
