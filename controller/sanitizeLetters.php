<?php
/**
 * Sanitizes the couples coords/letters sent by the user
 * 
 * @param type $board List of letters to sanitize, listed as 'coords'=>'letter'
 *      Example : [
 *                  '8_5'  => 'C',
 *                  '7_5'  => 'E',
 *                  '10_5' => 'O'
 *                  ]
 * @return array The same list with sanitized coords and sanitized letters
 */
function sanitizeLetters($board) {
    
    $result = [];
    
    foreach($board as $unsafeCoord=>$unsafeLetter) {
        $letter = filter_var($unsafeLetter, FILTER_SANITIZE_STRING);
        $coord  = filter_var($unsafeCoord, FILTER_VALIDATE_REGEXP, ['options' => ['regexp'=>'/[0-9]+_[0-9]+/'] ]);
        $result[$coord] = $letter;
    }
    
    return $result;
}
