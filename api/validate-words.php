<?php
/**
 * API to validate whether the new letters placed by the player are correct,
 * and return the new words formed
 */

require '../model/BoardReader.class.php';
require '../model/BoardWriter.class.php';
require '../model/ChatWriter.class.php';
require '../model/DictReader.class.php';
require '../controller/BoardValidator.class.php';
require '../controller/sanitizeLetters.php';


// Sanitize the data sent by the browser (the new letters placed)
$newLetters     = sanitizeLetters( json_decode($_POST['newLetters'], true) );
$boardReader    = new BoardReader();
// The board before the player placed his letters
$currentBoard   = $boardReader->getBoard();
$boardValidator = new BoardValidator($currentBoard, $newLetters);
$errorCode      = 'success';
$newWords       = null;
$wordsInDict    = null;
$wordsNotInDict = null;
$withBoardNeighbor   = null;
$notConnectedLetters = null;


if(empty($newLetters)) {
    // The player has clicked on "Popotamo" without placing any letter
    $errorCode = 'noNewLetters';
}
elseif($boardReader->areCellsVacant($currentBoard, $newLetters) === false) {
    $errorCode = 'lettersInOccupiedCells';
}
elseif(empty($currentBoard) and $boardReader->isLetterInCenter($newLetters) === false) {
    // The first letter of the game must cross the central cell
    $errorCode = 'mustStartInCenter';
}
elseif(empty($currentBoard) and count($boardValidator->futureBoard) === 1) {
    $errorCode = 'onlyOneLetter';
}
else {
    
    // If the player places the first letters of the game
    if(empty($currentBoard)) {
        // Search the words only from the central cell
        $centralCoords = $boardReader->getCentralStringCoords();
        $withBoardNeighbor[$centralCoords] = $newLetters[$centralCoords];
    }
    else {
        // Memorize the new letters placed by the player which touch letters placed during the previous turns
        $withBoardNeighbor = $boardReader->getNewLettersWithBoardNeighbor($currentBoard, $newLetters);
    }
    
    
    if($withBoardNeighbor !== null) {
        
        // Search the new words formed
        $newWords = $boardValidator->getNewWords($withBoardNeighbor);
        
        $dictReader = new DictReader();    
        // Validate the word with the dictionary
        foreach($newWords as $newWord) {

            if($dictReader->isInDict($newWord) === true) {
                $wordsInDict[] = $newWord;
            }
            else {
                $wordsNotInDict[] = $newWord;
                $errorCode = 'wordsNotInDict';
            }
        }
    }
    
    // Letters not connected to the already placed words
    $notConnectedLetters = $boardValidator->getNotConnectedLetters();
    $errorCode = ($notConnectedLetters === null) ? $errorCode : 'lettersNotConnected';
    
    
    if($errorCode === 'success') {
        $boardWriter = new BoardWriter();
        $chatWriter  = new ChatWriter();
        // If a letter is placed in the first or last colums/rows, 
        // all the letters are removed of the board (see Popotamo's rules)    
        $newBoard = $boardReader->isLetterInBorder($newLetters) ? [] : $boardValidator->futureBoard;
        // Save the new letters in the database/file
        $boardWriter->updateBoard($newBoard);
        // Save the new words in the chat history
        $chatWriter->addEventNewWords('Joueur inconnu', $newWords);
    }
}


echo json_encode([
    'errorCode' => $errorCode,
    'data'      => [
        'withBoardNeighbor'     => $withBoardNeighbor,
        'notConnectedLetters'   => $notConnectedLetters,
        'newWords'              => $newWords,
        'wordsInDict'           => $wordsInDict,
        'wordsNotInDict'        => $wordsNotInDict,
        ]
    ]);
