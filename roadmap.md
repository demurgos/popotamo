
Interface minimale : afficher la grille + un jeu de lettres préfédini. Pas de pioche ni multijoueurs ni sauvegarde.  
Pouvoir poser des lettres sur la grille  
Coder l'algorithme de validation des mots  
Pioche phase 1 : tirer une lettre selon sa fréquence. Pas de temps de pioche.  
Pioche phase 2 : temps de pioche/points d'action  
Conserver la partie même après actualisation de la page  
Permettre de jouer à plusieurs  
Permettre plusieurs parties simultanées  
Améliorer l'interface de pose de lettres (échanger, déplacer, annuler...)  
Appliquer l'interface graphique actuelle de Popotamo  
Les compétences/expérience  
Faire une 2e interface modernisée (responsive...)  

