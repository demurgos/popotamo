

LE POPODICO
(source : message de bouillegri sur http://www.popotamo.com/forum/thread/1326703#msg1326819)
2012


Vous l'avez vu dans la news, la MT met à notre disposition un nouvel outil de référence, le "Popodico", qui remplace désormais l'ODS.
Cet outil a été réalisé au bénéfice de Popotamo grâce à Muxxu et son support d'applications. Il a été conçu et mis au point par "l'architecteur" newSunshine qui a fait un travail remarquable, merci à lui :love:

A quoi sert le popodico ?

iI est depuis le 14 juillet l'arbitre qui détermine l'acceptation d'un mot.

Vous pourrez y accéder par le lien muxxu.com/a/popodico

Merci à newSunshine pour cet outil de sa fabrication :love:

NB :
En cas d'indisponibilité du popodico comme cela arrive parfois :
vous pouvez utiliser ce site : https://fr.scrabblecheat.org/anagramme , en n'oubliant pas de cocher ODS5